const { requestPromise } = require("../helpers/CustomRequest");
const { StatusError } =  require("../helpers/CustomError");
const errorHandler = require("../helpers/errorHandler");
const config = require("../config");

let apiOptions = config.apiOptions;

module.exports = (req, res, next) => {
    const respond = (result) => {
        if (!result.success || !result.token) throw new StatusError("DB 서버에 문제 발생", 500);
        req.token = result.token;
        next();
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    requestPromise({ email: config.email, password: config.password }, apiOptions.db_server + "/user/token")
        .then(respond)
        .catch(onError);
};