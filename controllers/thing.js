const jwt = require("jsonwebtoken");
const { requestPromise } = require("../helpers/CustomRequest");
const { StatusError } =  require("../helpers/CustomError");
const errorHandler = require("../helpers/errorHandler");
const config = require("../config");

let apiOptions = config.apiOptions;

// POST /thing
// input: name, key
// output: token 리턴값 없어도됨
//
// 1 - create
// input: name, token, require admin token
// output: token
// 2 - update
// input: token including id, require admin token
// output: token 리턴값 없어도됨
module.exports.create = function(req, res, next) {
    let adminToken = req.token;
    let createPath = "/thing?token=" + adminToken;
    let { name, key } = req.body;
    let secret = req.app.get("jwt-secret");

    let postData = {
        name: name,
        token: jwt.sign({
            name: name,
            key: key
        },
        secret, {
            issuer: "mital",
            subject: "thingInfo"
        })
    };

    const decodedToken = (result) => {
        return new Promise((resolve, reject) => {
            jwt.verify(result.token, secret, (err, decoded) => {
                if (err) reject(err);
                resolve({ id: result.id, decoded });
            });
        });
    };

    const updateToken = ({ id, decoded }) => {
        return new Promise((resolve, reject) => {
            jwt.sign({
                    _id: id,
                    name: decoded.name,
                    key: decoded.key
                },
                secret, {
                    issuer: "mital",
                    subject: "thingInfo"
                }, (err, token) => {
                    if (err) reject(err);
                    postData.token = token;
                    resolve(id);
                });
        });
    };
    const requestP = (id) => {
        let updatePath = "/thing/" + id + "?token=" + adminToken;
        return requestPromise(postData, apiOptions.db_server + updatePath, "PUT");
    };

    const respond = (result) => {
        res.json({
            success: result.success,
            message: "생성 완료",
            token: result.token
        });
    };

    const onError = (error) => {
        if (error.message === "존재하는 사물") {
            let tokenPath = "/thing/token?token=" + adminToken;
            requestPromise(postData, apiOptions.db_server + tokenPath)
                .then(decodedToken)
                .then(updateToken)
                .then(requestP)
                .then(respond)
                .catch(onError);
        }
        else errorHandler(error, req, res, next);
    };

    requestPromise(postData, apiOptions.db_server + createPath)
        .then(decodedToken)
        .then(updateToken)
        .then(requestP)
        .then(respond)
        .catch(onError);
};

// POST /thing/check
// input: name, key
// output: id
//
// 1 - request
// POST /thing/token?token=adminToken require admin token
// input: name
// output: token
// 2 - check token
// input: token, key
// output: id
module.exports.check = function(req, res, next) {
    let adminToken = req.token;
    let path = "/thing/token?token=" + adminToken;
    let { name, key } = req.body;
    let secret = req.app.get("jwt-secret");

    let postData = { name: name };

    const checkToken = (result) => {
        let token = result.token;
        return new Promise((resolve, reject) => {
            jwt.verify(token, secret, (err, decoded) => {
                if (err) reject(err);
                if (decoded.key === key) resolve(decoded._id);
                else reject(new StatusError("잘못된 키", 403));
            });
        });
    };

    const respond = (id) => {
        res.json({
            success: true,
            message: "키 확인",
            id: id
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    requestPromise(postData, apiOptions.db_server + path)
        .then(checkToken)
        .then(respond)
        .catch(onError);
};

// module.exports.update = function(req, res, next) {
//     res.render('index', { title: 'Express' });
// };
// module.exports.delete = function(req, res, next) {
//     res.render('index', { title: 'Express' });
// };
