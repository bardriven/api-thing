module.exports.StatusError = class StatusError extends Error {
    get status() {
        return this._status;
    }
    constructor(msg, status) {
        super(msg);
        this._status = status;
    }
};

