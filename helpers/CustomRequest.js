const request = require("request");
const { StatusError } = require("./CustomError");
const config = require("../config");

let apiOptions = config.apiOptions;


function requestPromise(postData, path, method = "POST") {
    let requestOptions = {
        url: path,
        method: method,
        json: postData,
        strictSSL: apiOptions.strictSSL
    };

    return new Promise((resolve, reject) => {
        request(requestOptions, (err, response, result) => {
            if (err) throw err;
            if (response.statusCode === 200 && result.success) {
                resolve(result);
            } else {
                reject(new StatusError(result.message, response.statusCode));
            }
        });
    });
}



module.exports.requestPromise = requestPromise;