const express = require("express");
const router = express.Router();
const controller = require("../controllers/thing");
const dbLogin = require("../middlewares/dbLogin");

/* GET home page. */
router.use(dbLogin);
router.post("/", controller.create);
router.post("/check", controller.check);
// router.put("/update", controller.update);
// router.delete("/delete", controller.delete);

module.exports = router;
