module.exports = {
    apiOptions: {
        db_server: "http://localhost:3001",
        strictSSL: false
    },
    email: "example@example.com",
    password: "example",
    secret: "SeCrEtKeYfOrHaShInG",
    mongodbURI: "mongodb://localhost:27017/AWS_IoT"
};